package com.company;
import java.util.*;

public class Main {

    public static double min(double x, double y)
    {
        double min = 0;
        if (x > y)
        {
            min = y;
        }
        else if (x < y)
        {
            min = x;
        }
        return min;
    }

    public static double max(double x, double y)
    {
        double max = 0;
        if (x > y)
        {
            max = x;
        }
        else if (x < y)
        {
            max = y;
        }
        return max;
    }

    public static void ascend(double a, double b, double c)
    {
        double cond1 = max(a, b);
        cond1 = max(cond1, c);
        double cond3 = min(a, b);
        cond3 = min(cond3, c);

        double cond2 = 0;

        if (cond1 != a && cond3 != a)
        {
            cond2 = a;
        }
        if (cond1 != b && cond3 != b)
        {
            cond2 = b;
        }
        if (cond1 != c && cond3 != c)
        {
            cond2 = c;
        }

        System.out.println("The numbers in ascending order are: " + cond3 + ", " + cond2 + ", " + cond1);
    }

    public static void checkvow(String a)
    {
        String vowels = "aeiou";
        if (vowels.contains(a))
        {
            System.out.println("The letter entered is a vowel");
        }
        else
        {
            System.out.println("The letter entered is a consonant");
        }
    }

    public static void oneto50()
    {
        for (int i = 0; i < 51; i++)
        {
            if (i % 7 != 0)
            {
                System.out.println(i);
            }
            else
            {
                continue;
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input the first number: ");
        double a = input.nextDouble();
        System.out.println("Input the second number: ");
        double b = input.nextDouble();

        double mini = min(a, b);
        if (mini == 0)
        {
            System.out.println("The numbers are equal");
        }
        else
        {
            System.out.println(mini + " is the minimum");
        }

        System.out.println("Input the first number: ");
        a = input.nextDouble();
        System.out.println("Input the second number: ");
        b = input.nextDouble();
        System.out.println("Input the third number: ");
        double c = input.nextDouble();

        ascend(a, b, c);

        System.out.println("Enter a letter to check if it's a vowel: ");
        String check = input.next();

        checkvow(check);

        oneto50();
    }
}
