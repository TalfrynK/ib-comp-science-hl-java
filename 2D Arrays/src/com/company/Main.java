package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int a[][] = new int[4][4];
        int b[][] = new int[4][4];

        a = getmatrix(input, a);

        System.out.println("Now for the second array");

        b = getmatrix(input, b); //Passing the input Scanner as a parameter

        int sum[][] = new int[4][4];

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                sum[i][j] = a[i][j] + b[i][j];
            }
        }
        
        for (int i = 0; i < 4; i++)
        {
            System.out.println("{ " + sum[i][0] + ", " + sum[i][1] + ", " + sum[i][2] + ", " + sum[i][3] + " }");
        }

    }

    public static int[][] getmatrix(Scanner input, int[][] a) {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                System.out.println("Input an int for " + i + ", " + j);
                a[i][j] = input.nextInt();
            }
        }

        return a;
    }
}
