package com.company;
import java.util.*;

public class Main {

    public static int[] inp(int x, int a[])
    {
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < x; i++)
        {
            System.out.println("Input an integer: ");
            a[i] = input.nextInt();
        }

        return a;
    }

    public static void search(int a[])
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter an integer to search for in the array: ");
        int search = input.nextInt();

        for (int i = 0; i < 10; i++)
        {
            if (search == a[i])
            {
                System.out.println("The entered integer was found in the array at position: " + i);
            }
        }
    }

    public static void checkstate(int length, int a[])
    {
        int positive = 0;
        int negative = 0;
        int even = 0;
        int odd = 0;

        for (int i = 0; i < length; i++)
        {
            if (a[i] < 0)
            {
                negative += 1;
            }
            if (a[i] > 0)
            {
                positive += 1;
            }
            if (a[i] % 2 == 0)
            {
                even += 1;
            }
            if (a[i] % 2 == 1)
            {
                odd += 1;
            }
        }

        System.out.println("Number of positive integers: " + positive);
        System.out.println("Number of negative integers: " + negative);
        System.out.println("Number of even integers: " + even);
        System.out.println("Number of odd integers: " + odd);
    }

    public static int[] reverse(int a[])
    {
        int[] b = new int[10];
        int j = 10;
        for (int i = 0; i < 10; i++) {
            b[j - 1] = a[i];
            j = j - 1;
        }
        return b;
    }

    public static void arraymin(int a[])
    {
        int len = a.length;
        double smallest;
        smallest = a[0];

        for (int i = 0; i < len; i++)
        {

            if (a[i] < smallest)
            {
                smallest = a[i];
            }
        }
        System.out.println("The smallest element of the array is: " + smallest);
    }

    public static void arraymax(int a[])
    {
        int len = a.length;
        double largest;
        largest = a[0];

        for (int i = 0; i < len; i++)
        {

            if (a[i] > largest)
            {
                largest = a[i];
            }
        }
        System.out.println("The largest element of the array is: " + largest);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] c = new int[10];
        c = inp(10, c);

        c = reverse(c);
        for (int i = 0; i < 10; i++)
        {
            System.out.println(c[i]);
        }

        arraymin(c);
        arraymax(c);

        int[] a = new int[10];
        a = inp(10, a);

        for (int i = 0; i < 10; i++)
        {
            System.out.println(a[i]);
        }

        search(a);

        int[] b = new int[20];
        b = inp(20, b);

        checkstate(20, b);



    }
}
