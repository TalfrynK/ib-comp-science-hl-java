package com.company;
import java.util.*;

public class Human {

    public static String firstname = "";
    public static String lastname = "";
    public static int age = 0;
    public static Scanner input = new Scanner(System.in);
    public static void collectinfo(Human a)
    {
        System.out.println("Input first name, last name, and then age:");
        a.firstname = input.next();
        a.lastname = input.next();
        a.age = input.nextInt();
    }
}
