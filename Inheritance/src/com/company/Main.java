package com.company;
import java.util.*;

final public class Main extends Student{

    public static int year;

    public void main(String[] args) {
        int x = Human.age + 5;
        Student y = new Student();
        Human.collectinfo(y);
        Student.StudentInfo(y);

        System.out.println("Student: " + y.firstname + " " + y.lastname + " " + y.age);
        System.out.println("In Year " + y.Year + " with an attendance of " + y.Attendance + "%");

        System.out.println(super.supertest);
    }
}
