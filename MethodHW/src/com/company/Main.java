package com.company;
import java.util.*;

public class Main {

    public static void square(double x){
        System.out.println(x * x);
    }

    public void square_root(double x){
        System.out.println(Math.sqrt(x));
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a number to square: ");
        double x = input.nextDouble();
        System.out.println("Enter a number to square root: ");
        double y = input.nextDouble();
        square(x);
        Main sqrt = new Main();
        sqrt.square_root(y);
    }
}
