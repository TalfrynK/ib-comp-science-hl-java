package com.company;
import java.util.*;

public class Main {

    public static double ShapeArea(double width, double length)
    {
        double area = width * length;
        return area;
    }

    public static double ShapeArea(double height, float base)
    {
        double area = 0.5 * height * base;
        return area;
    }

    public static double ShapeArea(double radius)
    {
        double area = radius * radius * 3.14;
        return area;
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double x = 5;
        double y = 10;
        float z = 15;
        double w = 20;

        double rectangle = ShapeArea(x, y);
        double triangle = ShapeArea(y, z);
        double circle = ShapeArea(w);

        System.out.println(rectangle + " : " + triangle + " : " + circle);
    }
}
