package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int mark = 0;
        int totalmark = 0;

        for (int i = 0; i < 5; i++)
        {
            System.out.print("Input the marks: ");
            mark = input.nextInt();

            totalmark = totalmark + mark;
        }

        double avg = totalmark / 5;
        String grade = "";

        if (avg >= 0) { grade = "E"; }
        if (avg >= 50) { grade = "D"; }
        if (avg >= 60) { grade = "C"; }
        if (avg >= 70) { grade = "B"; }
        if (avg >= 80) { grade = "A"; }
        if (avg >= 90) { grade = "A*"; }

        System.out.println("The total marks are " + totalmark + " out of 500. The average" +
                           " marks/percentage is " + avg + ", and the overall grade is " + grade + ".");

        System.out.println("Input a letter to check if it's a vowel or a consonant: ");
        String letter = input.next();
        letter.toLowerCase();
        String result = "Consonant";

        switch(letter)
        {
            case "a":
                result = "Vowel";

            case "e":
                result = "Vowel";

            case "i":
                result = "Vowel";

            case "o":
                result = "Vowel";

            case "u":
                result = "Vowel";
        }

        System.out.println("The letter entered is a " + result + ".");
    }
}
