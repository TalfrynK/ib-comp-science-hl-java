package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Choose the type of calculator you would like to use: 1 or 2: ");
        int type = input.nextInt();

        if (type == 1)
        {
            System.out.println("Enter +, -, *, /, or % to pick the operation you want: ");
            String choice = input.nextLine();

            System.out.print("How many numbers would you like to ");

            if (choice.equals("+"))
            {
                System.out.print("Enter integer 1: ");
                int num1 = input.nextInt();
                System.out.print("Enter integer 2: ");
                int num2 = input.nextInt();
                int num3 = num1 + num2;
                System.out.println(num3);
            }

            else if (choice.equals("-"))
            {
                System.out.print("Enter integer 1: ");
                int num1 = input.nextInt();
                System.out.print("Enter integer 2: ");
                int num2 = input.nextInt();
                int num3 = num1 - num2;
                System.out.println(num3);
            }

            else if (choice.equals("*"))
            {
                System.out.print("Enter integer 1: ");
                int num1 = input.nextInt();
                System.out.print("Enter integer 2: ");
                int num2 = input.nextInt();
                int num3 = num1 * num2;
                System.out.println(num3);
            }

            else if (choice.equals("/"))
            {
                System.out.print("Enter integer 1: ");
                int num1 = input.nextInt();
                System.out.print("Enter integer 2: ");
                int num2 = input.nextInt();
                int num3 = num1 / num2;
                System.out.println(num3);
            }

            else if (choice.equals("%"))
            {
                System.out.print("Enter integer 1: ");
                int num1 = input.nextInt();
                System.out.print("Enter integer 2: ");
                int num2 = input.nextInt();
                int num3 = num1 % num2;
                System.out.println(num3);
            }

            else
                System.out.print("Invalid Input");
        }

        else if (type == 2)
        {
            double num1 = 0;
            double finalnum = 0;
            String choice1 = "+";
            String oldchoice = "y";

            while (!choice1.equals("="))
            {
                System.out.print("Enter a number: ");
                num1 = input.nextDouble();
                oldchoice = choice1;
                System.out.println("Enter what symbol will go after this number (+, -, *, /, %) or '=' to end: ");
                choice1 = input.nextLine();
                String choice2 = oldchoice;

                if (choice2.equals("+"))
                {
                    finalnum = finalnum + num1;
                }
                if (choice2.equals("-"))
                {
                    finalnum = finalnum - num1;
                }
                if (choice2.equals("*"))
                {
                    finalnum = finalnum * num1;
                }
                if (choice2.equals("/"))
                {
                    finalnum = finalnum / num1;
                }
                if (choice2.equals("%"))
                {
                    finalnum = finalnum % num1;
                }
            }

            if (choice1.equals("="))
            {
                System.out.print(finalnum);
            }
        }

    }
}
