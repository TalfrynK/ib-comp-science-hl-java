package com.company;
import java.util.*;

import java.util.Scanner;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a number: ");
        double num = input.nextDouble();
        String response1 = "";
        String response2 = "";

        if (num > 0) {
            response1 = "The Number is Positive";
        }
        if (num < 0) {
            response1 = "The Number is Negative and hence not Prime";
        }
        else if (num == 0)
        {
            response1 = "The Number is Zero";
        }

        for (int i = 2; i < num/2; i++) {
            if (num % i == 0) {
                response2 = "The Number is not Prime";
            } else if (num == 1) {
                response2 = "The Number is One";
            } else {
                response2 = "The Number is Prime";
            }
        }

        System.out.println(response1);
        System.out.println(response2);
    }

}
